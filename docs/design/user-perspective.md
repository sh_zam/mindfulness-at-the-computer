


## User profiles

* User spends many hours per day in front of the computer
* User is stressed when using the computer
* User is disconnected from the body
  * User feels disconnected from emotions (her pain and joy, etc)
* User is not aware of the impact that her using the computer has on the world
* User is disconnected from other people (User is lonely)
* User is very concentrated at the computer screen
* User easily gets lost in the computer and is not aware of time

### Sensations

* Bodily tension because of the stress or excitement of solving a problem
* Dizziness after staring at the computer for long hours
* Sore wrists and shoulders



### Needs

* Reminders to take breaks after long hours of using the computer
* Reminders to maintain good posture sitting in front of the computer


## User Requirements

Please note the difference between user reqs and tech reqs

Req | Priority | Status
---|---|---
BREM | Essential | Implemented as breathing dialog popup |
RREM | High | Implemented as rest reminder and rest window |
FOLB | Essential | Implemented in the breathing dialog |
POSI | Medium | *Not covered*, could be covered by using a breathing phrase |
RGBY | Medium | *Not covered*, could be covered by using a rest action or general comment in the rest dialog |
RGPU | Low | *Not covered*, could be covered by using a breathing phrase |



### BREM - Reminders to breathe while using the computer

Showing messages/pop-ups for the user

The user can easily switch between single phrases that are shown to the user.
(If the user can easily switch between them it's less important to have a list of several phrases.)

### RREM - Remembering to rest

A dialog window can be shown to remind the user to take a break.
This window can maybe also be used to select the new practice (when the user gets back).
(Does this mean that we instead want to show the main window by "restoring" it?)

The application might ask a question like this: "Do you want to take a break?"; "Not right now"; "Yes".

A nice alternative to the above is to change the icon in the system tray area when it's time to
take a break, as this makes it easier for the user to choose when to take the break

There could also be an audio notification (maybe a bell).

### FOLB - Following the breath

Using the shift key (or pressing a button with three states) to follow the breath graphically

### POSI - Setting a positive intention

Remembering to be kind to oneself, doing things with "kindfulness"

Perhaps following up with questions later on

Example:
* Was I peaceful in my body and mind?

### RGBY - Remembering to take care of the body

A dialog window can remind the user to do some mindful movements for example.

### RGPU - Remembering the purpose of the work at the computer

Maybe we are contributing to social justice, mental well-being or something else, or perhaps
we are simply helping our colleagues?


## User scenarios

Also see the file flowcharts-and-wireframes.xml (draw.io)
